import { Action } from '@ngrx/store';
import * as actions from '../actions';
import * as types from '../action.types';
import { Odeljenje } from '../../models/odeljenje';

const initialState: Odeljenje = null;

export default function (state = initialState, action: Action ) {
    switch(action.type) {
        case types.CLASS_REQ_SUCCESS:{
            console.log('reducer jebeni', (action as actions.ClassRequestSuccess).odeljenje)
            return (action as actions.ClassRequestSuccess).odeljenje;
        }
        default: {
            return state;
        }
    }
}