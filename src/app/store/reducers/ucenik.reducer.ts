import { EntityState, EntityAdapter, createEntityAdapter } from "@ngrx/entity";
import { Ucenik } from "../../models/ucenik";
import { Action } from "@ngrx/store";
import * as actions from "../actions";
import {
  GET_STUDENTS,
  PUT_STUDENT,
  GET_STUDENTS_SUCCESS,
  PUT_STUDENT_SUCCESS
} from "../action.types";

export interface StudentsState extends EntityState<Ucenik> {}

const adapter: EntityAdapter<Ucenik> = createEntityAdapter<Ucenik>();

const initialState: StudentsState = {
  ids: [],
  entities: {}
};

// export interface StudentState{
//     studenti: Array<Ucenik>
// };

//const initialState: Array<Ucenik> = null;

//const initialState: Ucenik[] = [];

export default function(state: StudentsState = initialState, action: Action) {
  switch (action.type) {
    case GET_STUDENTS_SUCCESS: {
      const { students } = action as actions.GetStudentsSuccess;
      const clean = adapter.removeAll(state);
      return adapter.addMany(students, state);
      // console.log((action as actions.GetStudentsSuccess).students);
      // return (action as actions.GetStudentsSuccess).students;
    }
    default: {
      return state;
    }
  }
}

export const selectors = adapter.getSelectors();
