import { Action } from '@ngrx/store';
import * as actions from '../actions';
import * as types from '../action.types';

const initialState: string = "Lesson bro"

export default function (state = initialState, action: Action) {
    switch(action.type) {
        case types.LESSON_TYPED:{
            return ( action as actions.LessonTyped).lesson;            
        }
        default: {
            return state;
        }
    }
}
