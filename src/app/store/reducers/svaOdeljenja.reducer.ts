import { Action } from '@ngrx/store';
import * as actions from '../actions';
import * as types from '../action.types';
import { Odeljenje } from '../../models/odeljenje';

const initialState: Odeljenje[] = [];

export default function (state = initialState, action: Action ) {
    switch(action.type) {
        case types.GET_CLASSES:{
            return (action as actions.GetClasses).odeljenja;
        }
        default: {
            return state;
        }
    }
}