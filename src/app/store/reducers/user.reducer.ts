import {Action} from '@ngrx/store'
import { User } from '../../models/user';
import * as actions from '../actions'
import * as types from '../action.types'

const initialState: User = null

export default function(state: User = initialState, action: Action) {
    switch(action.type) {
        case types.AUTH_SUCCESSFUL:{
            console.log("USLI SMO U USER REDUCER AUTH SUCC");
            
            const {user} = action as actions.AuthSuccessful;
            alert("Login successful.");
            return user;

        }
        case types.LOGOUT:{
            alert("You are logged out.");
            return null;
        }
        default: {
            return state;
        }
    }
}