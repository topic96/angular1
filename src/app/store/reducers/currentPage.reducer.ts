import { Action } from '@ngrx/store';
import * as actions from '../actions';
import * as types from '../action.types';

const initialState: string = "main";

//const initialState: Student[] = [];

export default function (state = initialState, action: Action ) {
    switch(action.type) {
        case types.PAGE_CHANGED:{
            return (action as actions.PageChanged).page;
        }
        default: {
            return state;
        }
    }
}