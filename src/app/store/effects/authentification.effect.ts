import {Injectable} from '@angular/core'
import {Effect, Actions} from '@ngrx/effects'
import { AuthService } from '../../services/authentification.service';
import { AUTH_START, AUTH_SUCCESSFUL } from '../action.types';
import { map, switchMap, catchError } from 'rxjs/operators';
import {AuthStart, AuthSuccessful} from '../actions'

@Injectable()
export class AuthEffects{
    constructor(public actions$: Actions, public services: AuthService){
        
    }

    @Effect()
    login$ = this.actions$.ofType(AUTH_START)
            .pipe(
                map(userinfo => 
                    ({
                        usename:(userinfo as AuthStart).username,
                        password:(userinfo as AuthStart).password
                    })),
                switchMap(user=>{
                    return this.services.getUser(user.usename, user.password)
                    .pipe(
                        map(user=> new AuthSuccessful(user)),
                        catchError(()=>{
                            alert('Failed to login. efekat')
                            return []
                        })
                    )
                })
            )

}

