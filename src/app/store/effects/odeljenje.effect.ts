import { Injectable } from '@angular/core'
import { Effect, Actions } from '@ngrx/effects'
import { CLASS_REQ, CLASS_REQ_SUCCESS, AUTH_SUCCESSFUL } from '../action.types';
import { map, switchMap, catchError } from 'rxjs/operators';
import { ClassRequest, ClassRequestSuccess, GetClasses } from '../actions';
import { ClassService } from '../../services/odeljenje.service';

@Injectable()
export class OdeljenjeEffects{
    constructor(public actions$: Actions, public service: ClassService){
        
    }

    @Effect()
    getClasses$ = this.actions$
        .ofType(AUTH_SUCCESSFUL)
        .pipe(            
            switchMap( () => {
                return this.service.getClasses()
                    .pipe(
                        map(odeljenja => new GetClasses(odeljenja))
                    )
            })
        )

    @Effect()
    getClass$ = this.actions$
        .ofType(CLASS_REQ)
        .pipe(
            map(idInfo => (idInfo as ClassRequest).id),
            switchMap( id => {
                //console.log(id)
                return this.service.getClass(id)
                    .pipe(
                        map(odeljenje => 
                            new ClassRequestSuccess(odeljenje)
                        )
                    )
            })
        )

}

