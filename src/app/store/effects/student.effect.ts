import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";
import { StudentService } from "../../services/student.service";
import { GET_STUDENTS, PUT_STUDENT } from "../action.types";
import { map, switchMap, catchError } from "rxjs/operators";
import * as actions from "../actions";
//import { toPayload } from "@ngrx/effects";

@Injectable()
export class StudentEffects {
  constructor(private actions$: Actions, private service: StudentService) {}

  @Effect()
  get$ = this.actions$.ofType(GET_STUDENTS).pipe(
    map(ucenikInfo => (ucenikInfo as actions.GetStudents).id),
    switchMap(id => {
      return this.service.getStudents(id).pipe(
        map(students => {
          console.log(students, "wtffff");
          return new actions.GetStudentsSuccess(students);
        })
      );
    })
  );

  @Effect()
  put$ = this.actions$.ofType(PUT_STUDENT).pipe(
    map(ucenikInfo => (ucenikInfo as actions.PutStudent).ucenik),
    switchMap(ucenik => {
      console.log(ucenik);
      return this.service
        .putStudent(ucenik)
        .pipe(map(student => new actions.GetStudentsSuccess(student)));
    })
  );
}
