import { Action } from "@ngrx/store";
import * as types from "./action.types";
import { User } from "../models/user";
import { Ucenik } from "../models/ucenik";
import { Odeljenje } from "../models/odeljenje";

export class AuthStart implements Action {
  type = types.AUTH_START;

  constructor(public username: string, public password: string) {
    console.log("Usli u auth start");
  }
}

export class AuthSuccessful implements Action {
  type = types.AUTH_SUCCESSFUL;
  user: User;
  constructor(user: User) {
    this.user = user;
  }
}

export class Logout implements Action {
  type = types.LOGOUT;
}

export class PageChanged implements Action {
  type = types.PAGE_CHANGED;
  constructor(public page: string) {}
}

export class GetStudents implements Action {
  type = types.GET_STUDENTS;
  constructor(public id: number) {}
}

export class GetStudentsSuccess implements Action {
  type = types.GET_STUDENTS_SUCCESS;
  constructor(public students: Ucenik[]) {}
}

export class PutStudent implements Action {
  type = types.PUT_STUDENT;
  constructor(public ucenik: Ucenik) {}
}

export class PutStudentSuccess implements Action {
  type = types.PUT_STUDENT_SUCCESS;
  constructor(public ucenik: Ucenik) {}
}

export class ClassRequest implements Action {
  type = types.CLASS_REQ;

  constructor(public id: number) {}
}

export class ClassRequestSuccess implements Action {
  type = types.CLASS_REQ_SUCCESS;

  constructor(public odeljenje: Odeljenje) {
    console.log("Upad u class req success ", odeljenje);
  }
}

export class GetClasses implements Action {
  type = types.GET_CLASSES;

  constructor(public odeljenja: Odeljenje[]) {}
}

export class LessonTyped implements Action {
  type = types.LESSON_TYPED;
  constructor(public lesson: string) {
    console.log("Upad u lesson typed", lesson);
  }
}
