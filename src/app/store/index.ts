import { Ucenik } from "../models/ucenik";
import { ActionReducerMap } from "@ngrx/store";
import ucenikReducer, { StudentsState } from "./reducers/ucenik.reducer";
import currentPageReducer from "./reducers/currentPage.reducer";
import lessonTypedReducer from "./reducers/naslov.reducer";
import { User } from "../models/user";
import userReducer from "./reducers/user.reducer";
import { Odeljenje } from "../models/odeljenje";
import odeljenjeReducer from "./reducers/odeljenje.reducer";
import svaOdeljenjaReducer from "./reducers/svaOdeljenja.reducer";

export interface State {
  user: User;
  ucenici: StudentsState;
  currentPage: string;
  currentClass: Odeljenje;
  allClasses: Odeljenje[];
  lessonTyped: string;
}

export const rootReducer: ActionReducerMap<State> = {
  user: userReducer,
  ucenici: ucenikReducer,
  currentPage: currentPageReducer,
  currentClass: odeljenjeReducer,
  allClasses: svaOdeljenjaReducer,
  lessonTyped: lessonTypedReducer
};
