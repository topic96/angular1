import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { State } from '../../store';
import { PageChanged, Logout } from '../../store/actions';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public user$: Observable<User>
  public user: User
  
  constructor(private store$: Store<State>,
    public loginDialog: MatDialog, 
    public registerDialog: MatDialog) {
      this.user$ = this.store$.select(state => state.user)
      this.user$.subscribe(user => this.user = user)
   }

  ngOnInit() {
  }

  loadLoginPage(){
    this.store$.dispatch(new PageChanged('login'))
  }

  logout(){
    this.store$.dispatch(new Logout())
    this.store$.dispatch(new PageChanged('main'))
  }


  loadHomePage(){
    this.store$.dispatch(new PageChanged('home'))

  }

}
