import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { GetStudents, PageChanged, ClassRequest, LessonTyped } from '../../store/actions';
import { Observable } from 'rxjs';
import { Odeljenje } from '../../models/odeljenje';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  //public currClass: string
  //public currClass$ : Observable<string>

  public odeljenja$: Observable<Odeljenje[]>
  public odeljenje: Odeljenje
  public odeljenjeID: number
  //public lekcija

  // public odeljenja = [
  //   { "naziv": "I-1"},{ "naziv": "I-2"},{"naziv": "I-3"},{"naziv": "I-4"}
  // ]

  constructor(public store$: Store<State>) { 
    //this.currClass$ = this.store$.select(state => state.currentClass)
    this.odeljenja$ = this.store$.select(state => state.allClasses)
  }

  ngOnInit() {
  }

  selectedValue($event) {
    //console.log($event.value)
    console.log($event.value)
    this.odeljenjeID = $event.value
    //console.log(this.odeljenje)
  }

  // onTypeChange(typedValue: string) {
  //   console.log(typedValue);
  //   this.lekcija = typedValue;
  // }

  onNext($event) {
    $event.preventDefault()
    const title = $event.target.elements.title.value
    const content = $event.target.elements.content.value
    if(title !=='' && content !=='' ){
      this.store$.dispatch(new ClassRequest(this.odeljenjeID));
      this.store$.dispatch(new LessonTyped(title));
      //console.log(title)
      this.store$.dispatch(new GetStudents(this.odeljenjeID));
      this.store$.dispatch(new PageChanged("table"));
    }else {
      alert('Unesite naslov i komentar')
    }       
  }
}
