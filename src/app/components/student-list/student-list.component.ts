import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Ucenik } from "../../models/ucenik";
import { Store } from "@ngrx/store";
import { State } from "../../store";
import { Observable } from "rxjs";
import { GetStudents, PutStudent } from "../../store/actions";
import { Odeljenje } from "../../models/odeljenje";
import { Rubrika } from "../../models/rubrika";
import { User } from "../../models/user";
import { selectors as StudentSelectors } from "../../store/reducers/ucenik.reducer";

@Component({
  selector: "app-student-list",
  templateUrl: "./student-list.component.html",
  styleUrls: ["./student-list.component.css"]
})
export class StudentListComponent implements OnInit {
  public user: User;
  public user$: Observable<User>;

  public lesson: string;
  public lesson$: Observable<string>;

  public currClass: Odeljenje;
  public currClass$: Observable<Odeljenje>;

  public ucenici: Ucenik[];
  public ucenici$: Observable<Ucenik[]>;

  public rubrika: Rubrika;
  public subjects: any;

  constructor(public store$: Store<State>) {
    this.lesson$ = this.store$.select(state => state.lessonTyped);
    this.lesson$.subscribe(lesson => (this.lesson = lesson));
    this.subjects = {
      matematika: "Matematika",
      srpskiJezik: "Srpski Jezik",
      informatika: "Informatika"
    };

    this.ucenici$ = this.store$.select((state: State) =>
      StudentSelectors.selectAll(state.ucenici)
    );
    this.ucenici$.subscribe(ucenici => (this.ucenici = ucenici));

    this.user$ = this.store$.select(state => state.user);
    this.user$.subscribe(user => (this.user = user));

    this.currClass$ = this.store$.select(state => state.currentClass);
    this.currClass$.subscribe(currClass => (this.currClass = currClass));
    console.log(this.currClass);
    //console.log(this.currClass, "CURRCLASS")
    //console.log(this.lesson, "mdfk")
  }

  ngOnInit() {
    //this.store$.dispatch(new GetAllStudents()) //this.currClass
  }

  rubrikaKeys(rubrika) {
    return Object.keys(rubrika).filter(predmet => {
      return predmet == this.user.predmet;
    });
  }

  dodajOcenu($event) {
    $event.preventDefault();
    const ocena = $event.target.elements.ocena.value;
    const ucenikID: number = parseInt(
      $event.target.elements.ocena.previousSibling.innerText
    );
    const noviUcenik = this.ucenici.find(u => u.id == ucenikID);
    noviUcenik.rubrika[this.user.predmet].push(parseInt(ocena));

    //console.log(noviUcenik);

    if (ocena !== "") {
      this.store$.dispatch(new PutStudent(noviUcenik));
      //console.log(title)
    } else {
      alert("Niste uneli ocenu!");
    }
  }
}
