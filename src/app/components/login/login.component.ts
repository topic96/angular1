import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { Observable } from 'rxjs';
import { User } from '../../models/user';
import { AuthStart, PageChanged } from '../../store/actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: User
  public user$ : Observable<User>
  constructor(public store$: Store<State>) {
    this.user$ = this.store$.select(state => state.user)
   }

  ngOnInit() {
  }

  handleLogin(event){
    const username = event.target.elements.username.value;
    const password = event.target.elements.password.value;
    console.log(username, password);
    if (username !== "" && password !== "") {
      this.store$.dispatch(new AuthStart(username, password))
      this.store$.dispatch(new PageChanged('main'))
      
    } else {
      alert('Failed to login. Check if credentials are good.')
      //this.store$.dispatch(new PageChanged('main'))

    }
    
  }

  goBack(){
    this.store$.dispatch(new PageChanged('main'))
  }

}
