import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from './store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public currentPage$: Observable<string>;
  public currentPage: string;

  constructor(
    private store$: Store<State>
  ) {
    this.currentPage$ = this.store$.select(state => state.currentPage);
    this.currentPage$.subscribe(currentPage => this.currentPage = currentPage);
  }
}