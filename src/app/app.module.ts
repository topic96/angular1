import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import "rxjs";
import { EffectsModule, Actions } from "@ngrx/effects";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule
} from "@angular/material";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { LoginComponent } from "./components/login/login.component";
import { routes } from "./routes";
import { StudentListComponent } from "./components/student-list/student-list.component";
import { LayoutModule } from "@angular/cdk/layout";
import { StoreModule } from "@ngrx/store";
import { rootReducer } from "./store";
import { HttpModule } from "@angular/http";
import { StudentService } from "./services/student.service";
import { StudentEffects } from "./store/effects/student.effect";
import { CommonModule } from "@angular/common";
import { AuthEffects } from "./store/effects/authentification.effect";
import { AuthService } from "./services/authentification.service";
import { StudentComponent } from "./components/student/student.component";
import { OdeljenjeEffects } from "./store/effects/odeljenje.effect";
import { ClassService } from "./services/odeljenje.service";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentListComponent,
    SidebarComponent,
    LoginComponent,
    StudentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    LayoutModule,
    HttpModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot([StudentEffects, AuthEffects, OdeljenjeEffects])
  ],
  providers: [Actions, StudentService, AuthService, ClassService],
  bootstrap: [AppComponent]
})
export class AppModule {}
