export class Rubrika{
    constructor( 
        public matematika: Array<number>,
        public srpskiJezik: Array<number>,
        public informatika: Array<number>
    ) {}
}