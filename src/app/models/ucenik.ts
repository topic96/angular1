import { Rubrika } from "./rubrika";

export class Ucenik {
    constructor(
        public id: number,
        public imePrezime: string,
        public rubrika: Rubrika
    ) {}
}