import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Odeljenje } from '../models/odeljenje';


@Injectable()
export class ClassService{
    private PATH = 'http://localhost:3000/'

    constructor(public http:Http){

    }

    getClasses(): Observable<Odeljenje[]> {
      return this.http.get(`${this.PATH}odeljenja`)
        .pipe(
          map(res => {
            const odeljenja = res.json()
            console.log('ODELJENJA', odeljenja)
            return odeljenja
          })
        )
    }

    getClass(id: number): Observable<Odeljenje> {
      console.log('ovo je id u servisu ',id);
      return this.http.get(`${this.PATH}odeljenja/${id}`)
        .pipe(
          map(res => {
            const odeljenje = res.json()
            console.log('uzimam odeljenje ', odeljenje)
            return odeljenje
          })
        )
    }
}