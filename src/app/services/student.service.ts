import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import "rxjs/add/operator/catch";
import { Ucenik } from "../models/ucenik";

@Injectable({
  providedIn: "root"
})
export class StudentService {
  private PATH = "http://localhost:3000/";
  constructor(private http: Http) {}

  getStudents(id: number): Observable<Ucenik[]> {
    console.log("upali smo u get ucenik servis");

    return this.http.get(`${this.PATH}ucenici?odeljenjeID=${id}`).pipe(
      map(res => {
        console.log(res, "damn");
        return res.json();
      })
    );
  }

  putStudent(noviUcenik: Ucenik): Observable<Ucenik[]> {
    return this.http
      .put(`${this.PATH}ucenici/${noviUcenik.id}`, noviUcenik)
      .pipe(map(res => res.json()));
    // const response = this.http
    //   .get(`${this.PATH}ucenici/${noviUcenik.id}`)
    //   .subscribe(data => {
    //     const ucenik = JSON.parse(data["_body"]);
    //     ucenik.rubrika[noviUcenik.predmet].push(parseInt(noviUcenik.ocena));
    //     console.log(noviUcenik.ocena);
    //     console.log(ucenik.rubrika[noviUcenik.predmet]);

    //     console.log(ucenik);
    //     console.log(`${this.PATH}ucenici/${noviUcenik.id}`);
    //     const xhr = new XMLHttpRequest();
    //     xhr.onreadystatechange = function() {
    //       console.log(this.responseText);
    //     };
    //     xhr.open("put", `${this.PATH}ucenici/${noviUcenik.id}`, true);
    //     xhr.send(JSON.stringify(ucenik));
    //     const res = this.http.put(
    //       `${this.PATH}ucenici/${noviUcenik.id}`,
    //       ucenik
    //     );
    //     // .pipe(map(() => ucenik));
    //   });
    // // .pipe(
    // //   map(res => {
    // //     console.log(res);
    // //     return res.json();
    // //   })
    // // );

    // return;
  }
}
